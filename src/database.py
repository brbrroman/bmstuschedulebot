import sqlite3 as sqlite


class Database:
    create_table_sql = '''
        CREATE TABLE users (user_id, group_name);
    '''

    read_group_sql = '''
        SELECT group_name FROM users
        WHERE user_id = (?);
    '''

    write_group_sql = '''
        INSERT INTO users VALUES (?,?);
    '''

    update_group_sql = '''
        UPDATE users
        SET group_name = (?)
        WHERE user_id = (?)
    '''

    delete_group_sql = '''
        DELETE FROM users
        WHERE user_id = (?);
    '''

    def __init__(self, db_name):
        self.db_name = db_name
        self.connection = self.connect(db_name)
        self.cursor = self.connection.cursor()

        self.create_table()

    @staticmethod
    def connect(db_name):
        try:
            return sqlite.connect(db_name, check_same_thread=False)
        except sqlite.Error as e:
            print('DB error encountered: \n')
            print(e)

    def create_table(self):
        try:
            self.cursor.execute(Database.create_table_sql)
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            print('DB error encountered: \n')
            print(e)

    def read_group(self, user_id):
        try:
            return self.cursor.execute(Database.read_group_sql, (user_id,)).fetchone()
        except sqlite.Error as e:
            print('DB error encountered: \n')
            print(e)

    def write_group(self, user_id, group_name):
        try:
            self.cursor.execute(Database.write_group_sql, (user_id, group_name))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            print('DB error encountered: \n')
            print(e)

    def update_group(self, user_id, group_name):
        try:
            self.cursor.execute(Database.update_group_sql, (group_name, user_id))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            print('DB error encountered: \n')
            print(e)

    def delete_group(self, user_id):
        try:
            self.cursor.execute(Database.delete_group_sql, (user_id,))
            self.connection.commit()
        except sqlite.Error as e:
            self.connection.rollback()
            print('DB error encountered: \n')
            print(e)
