import telebot
from os import getenv
from datetime import date

from parser import ScheduleParser
from exceptions import *
from database import Database

bot = telebot.TeleBot(getenv('BOTTOKEN'))
db = Database('bot_db')

# A keyboard with all commands, that are available to the user
commandPalette = telebot.types.ReplyKeyboardMarkup(True)
commandPalette.row('/group')
commandPalette.row('/today', '/tomorrow')
commandPalette.row('/week', '/next_week')
commandPalette.row('/reset')
commandPalette.row('/help')


# This will display the list of available commands
@bot.message_handler(commands=['start', 'help'])
def start(message):
    bot.send_message(parse_mode='html',
                     chat_id=message.chat.id,
                     text='Сейчас тебе доступны следующие команды:\n'
                          '/set_group - установить группу, расписание которой ты хочешь у меня узнать\n'
                          '/group - посмотреть имя установленной на данный момент группы\n'
                          '/today - расписание на сегодня\n'
                          '/tomorrow - раписание на завтра\n'
                          '/week - расписание на эту неделю\n'
                          '/next_week - расписание на следующую неделю\n'
                          '/reset - сбросить настройки бота\n\n'
                          'Помочь этому боту всегда можно <a href="https://gitlab.com/slender652/bmstuschedulebot/">здесь</a>',
                     reply_markup=commandPalette)


@bot.message_handler(commands=['set_group'])
def set_group(message):
    split = message.text.split(' ')

    if len(split) != 2:
        bot.send_message(chat_id=message.chat.id, text='Не могу понять что это за группа...\n'
                                                       'Используй формат: /set_group <имя_группы>')
        return

    group = split[1]
    try:
        recorded_group = check_group(message.from_user.id)
        if recorded_group != group:
            db.update_group(message.from_user.id, group)
    except GroupNotFoundException:
        db.write_group(message.from_user.id, group)

    bot.send_message(chat_id=message.chat.id, text='Твоя группа теперь: ' + group)


@bot.message_handler(commands=['group'])
def group(message):
    try:
        bot.send_message(chat_id=message.chat.id, text='Твоя группа: ' + check_group(message.from_user.id))
    except GroupNotFoundException:
        bot.send_message(chat_id=message.chat.id, text='Я не знаю из какой ты группы...\n'
                                                       'Пожалуйста, раскажи мне, используя команду /set_group')
        pass


# Reads the group name from a db by unique TG user id
def check_group(chat_id):
    group = db.read_group(chat_id)

    if group is None:
        raise GroupNotFoundException()

    return group[0]


@bot.message_handler(commands=['today', 'tomorrow'])
def day_schedule(message):
    try:
        group = check_group(message.from_user.id)
    except GroupNotFoundException:
        bot.send_message(chat_id=message.chat.id, text='Я не знаю из какой ты группы...\n'
                                                       'Пожалуйста, раскажи мне, используя команду /set_group')
        return

    parser = ScheduleParser('https://students.bmstu.ru/', group)

    if message.text == '/today':
        try:
            weekday = convert_weekday(date.today().weekday())
        except IncorrectWeekDayException:
            bot.send_message(chat_id=message.chat.id,
                             text='Про этот день на сайте нет информации')  # TODO: Надо бы реализовать как-то по-другому
            return
    elif message.text == '/tomorrow':
        try:
            weekday = convert_weekday(date.today().weekday() + 1)
        except IncorrectWeekDayException:
            bot.send_message(chat_id=message.chat.id,
                             text='Про этот день на сайте нет информации')  # TODO: Надо бы реализовать как-то по-другому
            return

    try:
        lessons = parser.parse_schedule_for_one_day(weekday)
    except CouldntParseException:
        bot.send_message(chat_id=message.chat.id, text='Ой! Я не могу найти такую группу на сайте университета :(')
        return
    except UnknownWeekTypeException:
        bot.send_message(chat_id=message.chat.id,
                         text='Ой! Я не знаю какя сейчас неделя - числитель или знаменатель :(')
        return

    if len(lessons) == 0:
        bot.send_message(chat_id=message.chat.id, text='Пар нет!')
        return

    bot.send_message(parse_mode='Markdown', chat_id=message.chat.id, text=make_day_schedule(lessons))


# Converts weekday from an integer to a string of 2 characters
def convert_weekday(weekday):
    if weekday == 0:
        converted_weekday = 'ПН'
    elif weekday == 1:
        converted_weekday = 'ВТ'
    elif weekday == 2:
        converted_weekday = 'СР'
    elif weekday == 3:
        converted_weekday = 'ЧТ'
    elif weekday == 4:
        converted_weekday = 'ПТ'
    elif weekday == 5:
        converted_weekday = 'СБ'
    else:
        raise IncorrectWeekDayException()

    return converted_weekday


# Makes a string out of a list of lessons
def make_day_schedule(lessons_list):
    if lessons_list[0].day == 'ПН':
        schedule = '*Понедельник:*\n'
    elif lessons_list[0].day == 'ВТ':
        schedule = '*Вторник:*\n'
    elif lessons_list[0].day == 'СР':
        schedule = '*Среда:*\n'
    elif lessons_list[0].day == 'ЧТ':
        schedule = '*Четверг:*\n'
    elif lessons_list[0].day == 'ПТ':
        schedule = '*Пятница:*\n'
    elif lessons_list[0].day == 'СБ':
        schedule = '*Суббота:*\n'
    else:
        raise IncorrectWeekDayException()

    for lesson in lessons_list:
        schedule += lesson.time + ': ' + lesson.name + '\n'

    return schedule


# This will parse the schedule and reply with a week's schedule
@bot.message_handler(commands=['week', 'next_week'])
def week_schedule(message):
    try:
        group = check_group(message.from_user.id)
    except GroupNotFoundException:
        bot.send_message(chat_id=message.chat.id, text='Я не знаю из какой ты группы...\n'
                                                       'Пожалуйста, раскажи мне, используя команду /set_group')
        return

    parser = ScheduleParser('https://students.bmstu.ru/', group)

    if message.text == '/next_week':
        next_week_flag = True
    else:
        next_week_flag = False

    try:
        (week_header, lessons) = parser.parse_schedule(next_week=next_week_flag)
    except CouldntParseException:
        bot.send_message(chat_id=message.chat.id, text='Ой! Я не могу найти такую группу на сайте университета :(')
        return
    except UnknownWeekTypeException:
        bot.send_message(chat_id=message.chat.id,
                         text='Ой! Я не знаю какя сейчас неделя - числитель или знаменатель :(')
        return

    try:
        schedule = make_week_schedule(week_header, lessons)
    except IncorrectWeekDayException:
        bot.send_message(chat_id=message.chat.id, text='Упс! Что-то пошло не так :(')
        return

    bot.send_message(parse_mode='Markdown', chat_id=message.chat.id, text=schedule)


# Make a pretty string out of a lessons list
def make_week_schedule(week_header, lessons_list):
    monday = ''
    tuesday = ''
    wednesday = ''
    thursday = ''
    friday = ''
    saturday = ''

    for lesson in lessons_list:
        if lesson.day == 'ПН':
            monday += lesson.time + ': ' + lesson.name + '\n'
        elif lesson.day == 'ВТ':
            tuesday += lesson.time + ': ' + lesson.name + '\n'
        elif lesson.day == 'СР':
            wednesday += lesson.time + ': ' + lesson.name + '\n'
        elif lesson.day == 'ЧТ':
            thursday += lesson.time + ': ' + lesson.name + '\n'
        elif lesson.day == 'ПТ':
            friday += lesson.time + ': ' + lesson.name + '\n'
        elif lesson.day == 'СБ':
            saturday += lesson.time + ': ' + lesson.name + '\n'
        else:
            # raise IncorrectWeekDayException() - странно, почему-то тут все ломается
            pass

    schedule_string = '_' + week_header + '_\n\n' + \
                      '*Понедельник:*\n' + monday + '\n' + \
                      '*Вторник:*\n' + tuesday + '\n' + \
                      '*Среда:*\n' + wednesday + '\n' + \
                      '*Четверг:*\n' + thursday + '\n' + \
                      '*Пятница:*\n' + friday + '\n' + \
                      '*Суббота:*\n' + saturday

    return schedule_string


@bot.message_handler(commands=['reset'])
def reset(message):
    db.delete_group(message.from_user.id)
    bot.send_message(chat_id=message.chat.id, text='Настройки сброшены')


print('Bot running...')
bot.polling()
