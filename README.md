# BMSTUScedule bot

## About
This bot serves a purpose of providing an up-to-date information on the schedule of the Bauman Moscow State University.

## Currently available commands
  - /set_group - sets the group name to display the schedule for
  - /today - view the schedule for today
  - /tomorrow - view the schedule for tomorrow
  - /week - view the schedule for the current week
  - /next_week - view the schedule for the next week
  - /reset - reset the bot's settings
  - /help - show the list of available commands (this list)

## Contact the author
If you have any suggestions or want to report a bug, feel free to contact the author of this project at any time on telegram: @slender652

## Support the project
You can donate to the project's author using these credentials:

Yandex.Money: 4100 1927 1080 937