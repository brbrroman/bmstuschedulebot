#!/bin/bash

pgrep "python3" > /dev/null
status=$?

if [ $status == 0 ]
then
  echo "Bot running, everything OK"
elif [ $status == 1 ]
then
  nohup python3 ~/bmstuschedulebot/src/bot.py &> ~/bot.log &
elif [ $status == 2 ]
then
  echo "Syntax error"
else
  echo "Fatal error"
fi
